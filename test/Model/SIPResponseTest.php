<?php
/**
 * SIPResponseTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  AnalyzeData\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Analysis API
 *
 * This is the analyzedata API SDK
 *
 * OpenAPI spec version: 1.0.0
 * Contact: j.mengerink@analyzedata.com
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.21
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace AnalyzeData\Client\Test\Model;

/**
 * SIPResponseTest Class Doc Comment
 *
 * @category    Class
 * @description SIPResponse
 * @package     AnalyzeData\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class SIPResponseTest extends \PHPUnit_Framework_TestCase {

	/**
	 * Setup before running any test case
	 */
	public static function setUpBeforeClass() {
	}

	/**
	 * Setup before running each test case
	 */
	public function setUp() {
	}

	/**
	 * Clean up after running each test case
	 */
	public function tearDown() {
	}

	/**
	 * Clean up after running all test cases
	 */
	public static function tearDownAfterClass() {
	}

	/**
	 * Test "SIPResponse"
	 */
	public function testSIPResponse() {
	}

	/**
	 * Test attribute "callId"
	 */
	public function testPropertyCallId() {
	}

	/**
	 * Test attribute "code"
	 */
	public function testPropertyCode() {
	}

	/**
	 * Test attribute "message"
	 */
	public function testPropertyMessage() {
	}
}
