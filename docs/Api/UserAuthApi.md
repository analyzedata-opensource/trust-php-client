# AnalyzeData\Client\UserAuthApi

All URIs are relative to *https://ingestion.analyzedata.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addUserAuth**](UserAuthApi.md#adduserauth) | **POST** /user-auth/async | send login metadata and expect result via callback

# **addUserAuth**
> addUserAuth($body)

send login metadata and expect result via callback

sends a single user authentication event to be analyzed

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = AnalyzeData\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new AnalyzeData\Client\API\UserAuthApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AnalyzeData\Client\Model\UserAuth(); // \AnalyzeData\Client\Model\UserAuth | User authentication event

try {
    $apiInstance->addUserAuth($body);
} catch (Exception $e) {
    echo 'Exception when calling UserAuthApi->addUserAuth: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AnalyzeData\Client\Model\UserAuth**](../Model/UserAuth.md)| User authentication event | [optional]

### Return type

void (empty response body)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

