# AnalyzeData\Client\SipAnalysisApi

All URIs are relative to *https://ingestion.analyzedata.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addCDR**](SipAnalysisApi.md#addcdr) | **POST** /cdr/async | send a CDR and expect result via callback

# **addCDR**
> addCDR($body)

send a CDR and expect result via callback

sends a single CDR record to be analyzer

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
// Configure HTTP basic authorization: basicAuth
$config = AnalyzeData\Client\Configuration::getDefaultConfiguration()
              ->setUsername('YOUR_USERNAME')
              ->setPassword('YOUR_PASSWORD');


$apiInstance = new AnalyzeData\Client\API\SipAnalysisApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \AnalyzeData\Client\Model\CDRRecord(); // \AnalyzeData\Client\Model\CDRRecord | User authentication event

try {
    $apiInstance->addCDR($body);
} catch (Exception $e) {
    echo 'Exception when calling SipAnalysisApi->addCDR: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\AnalyzeData\Client\Model\CDRRecord**](../Model/CDRRecord.md)| User authentication event | [optional]

### Return type

void (empty response body)

### Authorization

[basicAuth](../../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

