# DataPoint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | **map[string,string]** | allow you to specify any additional data for reference | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

