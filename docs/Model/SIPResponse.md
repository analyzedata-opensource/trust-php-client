# SIPResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**call_id** | **string** | The unique identifier of this call in the AnalyzeData infrastructure | [optional] 
**code** | **int** |  | [optional] 
**message** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

