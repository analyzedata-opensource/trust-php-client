# CDRRecord

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**call_id** | **string** | unique identifier of the call in your infrastructure. partial CDRs (e.g. TRYING, ANSWRED) posted with the same call_id, will be automatically merged. If a call_id is omitted, one will be generated server-side. | [optional] 
**src** | **string** | number making the call in E164 format | [optional] 
**dst** | **string** | number being called in E164 format | [optional] 
**disposition** | **string** | phase that the call is in | [optional] 
**user_agent** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

