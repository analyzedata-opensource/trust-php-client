# QuickStart



```php
$config = new Configuration();
$config->setHost("https://ingestion.analyzedata.com")
	->setUsername("your-project-number-here")
	->setPassword("YOUR-PROJECT-ACCESS-TOKEN-HERE");

$API = new SipAnalysisApi(null, $config);

$cdr = new CDRRecord([
    'call_id' => '55cc2800-07da-11eb-a83b-b1abfac34fbc',
    'src' => '+31401111111',
    'dst' => '+31452222222',
    'disposition' => 'TRYING'
]);

$API->addCDR($cdr);
```


# Details
For more details, please see the `docs/` folder. If you feel any documentation is unclear or lacking, please feel free to contact us at `info@analyzedata.com`, or simple create an issue on our GitLab:
https://gitlab.com/analyzedata-opensource/trust-php-client/-/issues